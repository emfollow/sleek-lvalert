# Changelog

## 2.0.1 (unreleased)

-   Speed up imports by up to a second by replacing uses of `pkg_resources`
    with the new Python standard library module `importlib.resources` (or, for
    Python < 3.7, the backport `importlib_resources`). The old `pkg_resources`
    module is known to be slow because it does a lot of work on startup. (See,
    for example, https://github.com/pypa/setuptools/issues/926 and
    https://github.com/pypa/setuptools/issues/510.)

## 2.0.0 (2020-03-11)

-   Switch XMPP client from SleekXMPP to Slixmpp. SleekXMPP is no longer
    maintained.

-   Bump minimum Python version to Python 3.5, since this is the minimum
    version supported by Slixmpp.

## 1.0.0 (2020-03-03)

-   Add a monkeypatch to work around an issue in SleekXMPP's parsing of dates
    in X.509 certificates that has been fixed in git but is not in a SleekXMPP
    release. This workaround allows us to unpin and depend on the latest
    versions of pyasn1 and pyasn1-modules.

-   Change license from GPL-2+ to GPL-3+.

## 0.0.5 (2019-02-01)

-   Catch and log all exceptions that occur in the LVAlert listener callback.

## 0.0.4 (2018-09-24)

-   Move netrc utility to an external package,
    [safe-netrc](https://pypi.org/project/safe-netrc/).

-   Add valid certificates for `lvalert.cgca.uwm.edu` and
    `lvalert-playground.cgca.uwm.edu`.

## 0.0.3 (2018-07-31)

-   Add `nodes` subcommand to list available pubsub nodes.

-   Include bundled copies of the servers' certificates and validate
    host certificates against them.

## 0.0.2 (2018-07-05)

-   The command line tool will no longer try to automatically reconnect
    when doing admin actions like subscribing and unsubscribing.

-   Add support for Python 2.7.

## 0.0.1 (2018-07-03)

-   Initial release.
