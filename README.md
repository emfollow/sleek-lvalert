# sleek-lvalert

sleek-lvalert is a client for the LIGO/Virgo LVAlert pubsub infrastructure that
is powered by [slixmpp](https://slixmpp.readthedocs.io). It requires Python 3.5
or later.
